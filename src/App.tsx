import React, { useEffect } from 'react'
import Axios from './Hooks/axios/configuration'
import ADialog from './components/Dialog/A-Dialog'
import { BrowserRouter as Router } from "react-router-dom";
import Rout from "./router";


function App() {
  const {req, simpleReq} = Axios({url: "https://api.publicapis.org", debug: false})

  useEffect(()=>{

    async function test(){
      let {status, data} = await req({method: "GET", url: '/categories'})
      console.log(status, data)
    }

    async function test1() {
      let {status, data} = await simpleReq(`/categories`, 'get')
      console.log(status, data);
    }
    
    test()
    test1()

  },[])


  return (
    <div className="App">
      <ADialog>
        <Router>
          <Rout />
        </Router>
      </ADialog>
    </div>
  );
}

export default App
