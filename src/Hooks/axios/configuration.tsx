import {useState, useMemo} from 'react'
import axios from 'axios'

interface AxiosHeaderProps {
    authorization: string,
    accept: "application/json",
    "content-type": "application/json"
}

interface AxiosResultProps {
    data: string | number | boolean | null | undefined | object | Error ,
    status: number | string
}
 
interface AxiosProps  {
    workspace?: string,
    url: string,
    headers?: AxiosHeaderProps,
    callbackResult?: (res: AxiosResultProps) => void,
    debug?: boolean
}

type method = "GET" | "POST" | "PUT" | "DELETE" | "PATCH" 
type simpleMethod =  "get" | "post" | "put" | "delete" | "patch"

interface AxiosRequestProps {
    method: method,
    url: string,
    body?: string | object | null | undefined | object[] | string[] | number[],
    headers?: AxiosHeaderProps
}

// interface requestFunc {
//     (requestParams : AxiosRequestProps) : Promise<AxiosResultProps>
// }

// interface simpleRequestFunc {
//     (url:string, method : simpleMethod, body?:string | object | null | undefined, headers?: AxiosHeaderProps) : Promise<AxiosResultProps>;
// }

type simpleRequestFunc =  (url:string, method : simpleMethod, body?:string | object | null | undefined, headers?: AxiosHeaderProps) => Promise<AxiosResultProps>;

let simpleReq: simpleRequestFunc;


export const useAxios = (props : AxiosProps) => {
    const [loading, setLoading] = useState(false)

    const defautlAxios = useMemo(()=> axios.create({
        baseURL: props.url
    }), [props.url])

    const setHeader = (headers: AxiosHeaderProps) : void => {
        defautlAxios.defaults.headers = headers
    }

    async function req (requestParams : AxiosRequestProps) : Promise<AxiosResultProps>{
        setLoading(true)

        const result = await defautlAxios(requestParams)
        const {status, data} = result

        if(props.debug){
            console.log(result)
        }

        setLoading(false)
        return { status , data }
    }

    simpleReq = async (url:string, method : simpleMethod, body?:string | object | null | undefined, headers?: AxiosHeaderProps) : Promise<AxiosResultProps> => {
        setLoading(true)

        const result = await defautlAxios[method](url,body, {headers}  )
        const {status, data} = result


        if(props.debug){
            console.log(result)
        }

        setLoading(false)
        return { status , data }

    }

    return {req, simpleReq, setHeader, loading, defautlAxios}

}

export default useAxios;


