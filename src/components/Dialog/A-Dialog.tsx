import React,{FC, useState, ReactNode, Dispatch, SetStateAction} from "react";
import {
  Dialog,
  DialogTitle,
  DialogContent,
  LinearProgress,
  DialogActions,
  Button
} from "@material-ui/core";

type Actions = {
    title : string,
    action(...args : []) : any | void
}

type Size = "xs" | "sm" | "md" | "lg" | "xl"

interface DialogConfig {
    title: string,
    subtitle?: string,
    readonly maxWidth?: Size,
    readonly fullWidth?: boolean,
    component?: ReactNode, 
    type?: string,
    dialogActions?: Actions[],
    actionCallback(payload: any): void,
    [propName: string]: any;
}

interface DialogContextInterface {
  openDialog(dialogParams : DialogConfig) : void,
  resetDialog() : void,
  onDismiss() : void,
  setLoading: Dispatch<SetStateAction<boolean>>,
  onConfirm(payload: any) : void
}


const DialogContext = React.createContext<DialogContextInterface>({} as DialogContextInterface)


const ADialog : FC = ({ children }) => {

    const [dialogOpen, setDialogOpen] = useState<boolean>(false)
    const [dialogConfig, setDialogConfig] = useState<DialogConfig>({} as DialogConfig)
    const [loading, setLoading] = useState<boolean>(false)

    const openDialog = (dialogParams : DialogConfig) : void => {
        setDialogOpen(true);
        setDialogConfig(dialogParams);
    }

    const resetDialog = () : void => {
        setDialogOpen(false);
        setLoading(false)
        dialogConfig.actionCallback(false);
        setDialogConfig({} as DialogConfig);
    }

    const onConfirm = (payload : any) : void => {
        dialogConfig.actionCallback(payload)
    }

    const onDismiss = () : void => {
        resetDialog();
        setLoading(false);
        dialogConfig.actionCallback(false)
    };

    const renderDialogContent = () : ReactNode => {
        return <div>Salam</div>
    }


    return (
      <DialogContext.Provider
        value={{ openDialog, resetDialog, onDismiss, setLoading, onConfirm }}
      >
        <Dialog
          maxWidth={dialogConfig.maxWidth}
          fullWidth={dialogConfig.fullWidth}
          open={dialogOpen}
          onClose={() => resetDialog()}
        >
          {loading && <LinearProgress />}
          <DialogTitle>{dialogConfig.title}</DialogTitle>
          <DialogContent dividers>{renderDialogContent()}</DialogContent>
          {dialogConfig.dialogActions && (
            <DialogActions>
              {dialogConfig.dialogActions?.map((action: Actions) => (
                <Button color="secondary" onClick={action.action}>
                  {action.title}
                </Button>
              ))}
            </DialogActions>
          )}
        </Dialog>
        {children}
      </DialogContext.Provider>
    );
}

export const useDialogHandler = () : any => {
  const { openDialog, resetDialog, onDismiss, setLoading } =
    React.useContext(DialogContext);

  const dialogHandler = ( dialogPayload : DialogConfig ) : Promise<DialogContextInterface> =>
    new Promise((resolve) => {
      openDialog({
        ...dialogPayload,
        actionCallback: resolve,
        title: dialogPayload.title,
      });
    });

  return { dialogHandler, resetDialog, onDismiss, setLoading };
};


export default ADialog