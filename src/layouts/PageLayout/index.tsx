import React from 'react';
import { Container, makeStyles } from '@material-ui/core';
import { Outlet } from 'react-router-dom';

const useStyles = makeStyles(() => ({
  root: {
    display: 'flex',
    minHeight: '100%',
    overflow: 'hidden',
    width: '100%',
    padding: 0,
    right: 0,
    left: 0,
    top: 0
  },
  content: {
    padding: 0,
    margin: 0,
    display: 'flex',
    flex: '1 1 auto',
    overflow: 'hidden',
    width: '100%',
  }
}));

function PageLayout() {
  const classes = useStyles();

  return (
    <Container maxWidth="sm" className={classes.root} style={{ height: '100%' }}>
      <Outlet />
    </Container>
  );
}
export default PageLayout;
