import React  from 'react';
import { useRoutes } from 'react-router-dom';
import PageLayout from '../layouts/PageLayout';

import Home from '../views/Home'

const App = () => {
    const routes = useRoutes( [
        {
            path: '/',
            element: <PageLayout />,
            children: [
                { path: '/', element: <Home /> }
            ]
        }
    ] );
    return routes;
};

export default App;
